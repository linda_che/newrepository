antlr4 -no-listener src/com/example/magnidemo/engine/DotsLines.g4
javac -d bin/ src/com/example/magnidemo/engine/*.java

cd bin
grun com.example.magnidemo.engine.DotsLines lines
