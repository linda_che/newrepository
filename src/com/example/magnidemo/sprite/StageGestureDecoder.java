package com.example.magnidemo.sprite;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Decode touch event into user interaction event
 * @author Tian Yu
 * @version 0.3
 *
 */
public class StageGestureDecoder implements GestureDetector.OnGestureListener {

	public static final String TAG = StageGestureDecoder.class.getSimpleName();

	Stage mStage;

	public StageGestureDecoder(Stage stg) {
		mStage = stg;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onDown");
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onFling");
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

		Log.d(TAG, "onLongPress");
		// mStage.longPressDetected(e.getX(0), e.getY(0));

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {

		Log.d(TAG, "onScroll");
		mStage.moving(e2.getX(0), e2.getY(0));
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {

		Log.d(TAG, "onShowPress");
		mStage.longPressDetected(e.getX(0), e.getY(0));
		mLPD = true;
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {

		if (!mLPD) {
			Log.d(TAG, "onSingleTapUp");
			mStage.clicked(e.getX(0), e.getY(0));
		} else {
			mLPD = false;
		}
		return false;
	}

	boolean mLPD = false;

}
