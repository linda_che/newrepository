package com.example.magnidemo.sprite;

import com.example.magnidemo.R;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;

/**
 * Represents a dot
 * TODO There should be sprite hierarchy
 * @author Tian Yu, Cao Xiying
 * @version 0.3
 *
 */
public class Dot {

	public static final String TAG = "Sprite.Dot";

	public Point mCenter = new Point();

	Bitmap mBitmap;
	boolean mPaired = false;

	// TODO no use?
	int mRadius = 20;

	Stage mStage;

	public Dot() {
	}

	public void setPaired(boolean b) {
		mPaired = b;
	}

	public boolean isPaired() {
		return mPaired;
	}

	public void setStage(Stage stg) {
		mStage = stg;
		mBitmap = mStage.mBitmapPool.getBitmapFor(R.drawable.ic_dot_30);
	}

	/**
	 * @param canvas
	 * @param leftX
	 * @param topY
	 * @param dotSize
	 *            size of the dot depends on depth.
	 */
	public void draw(Canvas canvas) {
		canvas.drawBitmap(mBitmap, mCenter.x - mBitmap.getWidth() / 2,
				mCenter.y - mBitmap.getHeight() / 2, null);
	}

}
