package com.example.magnidemo.sprite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.example.magnidemo.R;
import com.example.magnidemo.engine.Core;

/**
 * Represents the stage for all sprites, etc.
 * 
 * @author TianYu
 * @version 0.3
 * 
 */
public class Stage extends View {
    // private static final String TAG = "Measurement.Stage";

    private Bitmap mBitmap;
    private Magnifier mMagnifier;

    public final BitmapPool mBitmapPool = new BitmapPool(getResources());

    Core mCore = new Core();
    Collision mCollision = new Collision(mCore);

    GestureDetector mGD;
    GestureDetector.OnGestureListener mGL;

    private Rect mViewRect = new Rect();
    private Rect mBitmapDisplayRect = new Rect();

    protected Stage(Context context) {
        super(context);
        init(context);
    }

    public Stage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * one time init after created
     * 
     * @param context
     */
    public void init(Context context) {

        mCore.setStage(this);

        mGL = new StageGestureDecoder(this);
        mGD = new GestureDetector(context, mGL);
        mGD.setIsLongpressEnabled(false);
    }

    public boolean setDataSource(int res) {
        boolean ret = true;

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.show);

        return ret;
    }

    public boolean setDataSource(String filename) {
        boolean ret = true;

        mBitmap = BitmapFactory.decodeFile(filename);

        return ret;
    }

    public boolean setDataSource(Uri uri) {
        boolean ret = true;

        // TODO should load image as gallery
        // mBitmap = BitmapFactory.decodeFile(mImageName);

        return ret;
    }

    public boolean prepare() {

        if (mBitmap == null) {
            return false;
        }

        mMagnifier = Magnifier.create(PseudoCommonSettings.MagnifierRadius, PseudoCommonSettings.MagnifierFactor,
                mBitmap);
        mMagnifier.setStage(this);
        mMagnifier.setVisible(false);

        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getActionMasked()) {
        case android.view.MotionEvent.ACTION_UP:
            // TODO if showing mag
            hideMag();
            if (mMovingDot) {
                endMovingDot();
            }

            // TODO this is a work around
            this.invalidate();
            break;
        }

        // dispatch gesture
        // dispatchGesture(x, y);
        mGD.onTouchEvent(event);
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            mViewRect.set(left, top, right, bottom);
            updateBitmapDisplayRect(mBitmapDisplayRect, mViewRect, mBitmap);
            mMagnifier.setRects(mBitmapDisplayRect, mViewRect);
        }
    }

    private void updateBitmapDisplayRect(Rect bdr, Rect vr, Bitmap bmp) {
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();
        int vrWidth = vr.width();
        int vrHeight = vr.height();

        float bmpRatio =  bmpWidth / (float)bmpHeight;
        float vrRatio = vrWidth / (float)vrHeight;

        int bdrWidth, bdrHeight;

        if (bmpRatio <= vrRatio) {
            // thin
            bdrHeight = vrHeight;
            bdrWidth = Math.round(vrHeight * bmpRatio);
        } else {
            // fat
            bdrWidth = vrWidth;
            bdrHeight = Math.round(vrWidth / bmpRatio);
        }

        int left, top, right, bottom;

        int hdW = (vrWidth - bdrWidth) / 2;
        int hdH = (vrHeight - bdrHeight) / 2;
        
        left = hdW;
        top = hdH;
        right = vrWidth - hdW;
        bottom = vrHeight - hdH;

        bdr.set(left, top, right, bottom);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // background
        if (mBitmap != null) {
            canvas.drawBitmap(mBitmap, (Rect)null, mBitmapDisplayRect, null);
        }

        // dots and lines in FIFO order, and link before points within a line
        for (Line l : mCore.mLineSet) {
            l.draw(canvas);
        }

        for (Dot d : mCore.mDotStack) {
            d.draw(canvas);
        }

        // magnifier
        if (mMagnifier != null) {
            mMagnifier.draw(canvas);
        }
    }

    private void showMag(int x, int y) {

        if (mMagnifier != null) {
            mMagnifier.setVisible(true);
            mMagnifier.aimAtViewRect(x, y);
        }
    }

    private void hideMag() {

        if (mMagnifier != null) {
            mMagnifier.setVisible(false);
        }
    }

    // user ops
    public void clicked(float fx, float fy) {
        int x = Math.round(fx);
        int y = Math.round(fy);

        Object o = mCollision.hit(x, y);
        if (o instanceof Dot) {
            mCore.removeDot((Dot) o);
        } else if (o instanceof Line) {
            // don't care
        } else if (o == null) {
            mCore.addDot(x, y);
        }

    }

    public void longPressDetected(float fx, float fy) {
        int x = Math.round(fx);
        int y = Math.round(fy);
        Object o = mCollision.hit(x, y);

        if (o instanceof Dot) {
            startMovingDot((Dot) o);
        } else if (o instanceof Line) {
            // TODO should show flexmenu
            mCore.removeLine((Line) o);
        } else if (o == null) {

            Object j = mCore.addDot(x, y);
            if (j instanceof Dot) {
                Line l = (Line) mCore.addDot(x + 1, y + 1);
                startMovingDot(l.mDots[1]);
            } else if (j instanceof Line) {
                startMovingDot(((Line) j).mDots[1]);
            }
        }
    }

    Dot mMovingThisDot = null;
    boolean mMovingDot = false;

    private void startMovingDot(Dot d) {
        mMovingThisDot = d;
        mMovingDot = true;
    }

    private void endMovingDot() {

        mMovingThisDot = null;
        mMovingDot = false;
    }

    public void moving(float fx, float fy) {
        int x = Math.round(fx);
        int y = Math.round(fy);
        if (mMovingDot) {
            showMag(x, y);
            mCore.updateDot(mMovingThisDot, x, y);
        }
    }

    public void clear() {
        mCore.removeAll();
    }
}
