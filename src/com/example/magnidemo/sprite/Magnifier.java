package com.example.magnidemo.sprite;

import com.example.magnidemo.R;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;

/**
 * Represents the magnifier TODO There should be sprite hierarchy
 * 
 * @author TianYu
 * @version 0.3
 * 
 */
public class Magnifier {

    // TODO radius should be calculated from frame drawable size, or read from settings
    private int mRadius = PseudoCommonSettings.MagnifierRadius;
    private int mFactor = PseudoCommonSettings.MagnifierFactor;

    private Rect mViewRect = new Rect();
    private Rect mBitmapDisplayRect = new Rect();
    private float mBitmapDisplayScaleRatio = 1f;
    private boolean mThin;

    private Bitmap mMagnifierFrame;
    private Point mAimPoint4BitmapDisplayRect = new Point();
    private Point mAimPoint4ViewRect = new Point();
    private Point mCenter4ViewRect = new Point();

    private Bitmap mBitmap;
    private ShapeDrawable mDrawable;
    private Matrix mShaderLocalMatrix = new Matrix();

    boolean mVisible = false;
    Stage mStage;

    // 2 S C F
    public static Magnifier create(int radius, int factor, Bitmap bitmap) {
        Magnifier obj = new Magnifier(radius, factor, bitmap);

        if (!obj.init()) {
            return null;
        }
        return obj;
    }

    protected Magnifier(int radius, int factor, Bitmap bitmap) {
        mRadius = radius;
        mFactor = factor;
        mBitmap = bitmap;
    }

    public boolean init() {
        boolean ret = true;
        if (mBitmap == null) {
            ret = false;
        } else {
            try {
                BitmapShader shader = new BitmapShader(Bitmap.createScaledBitmap(mBitmap, mBitmap.getWidth() * mFactor,
                        mBitmap.getHeight() * mFactor, true), TileMode.CLAMP, TileMode.CLAMP);

                mDrawable = new ShapeDrawable(new OvalShape());
                mDrawable.setBounds(0, 0, mRadius * 2, mRadius * 2);
                mDrawable.getPaint().setShader(shader);

            } catch (Exception e) {
                ret = false;
            }
        }
        return ret;
    }

    public void setStage(Stage stg) {
        mStage = stg;
        mMagnifierFrame = mStage.mBitmapPool.getBitmapFor(R.drawable.mf);
        aimAtViewRect(10, 10);
    }

    public void setRects(Rect bdr, Rect vr) {
        mBitmapDisplayRect = bdr;
        mViewRect = vr;

        mThin = mBitmapDisplayRect.height() == mViewRect.height() ? true : false;
        if (mThin) {
            mBitmapDisplayScaleRatio = mBitmapDisplayRect.height() / (float) mBitmap.getHeight();
        } else {
            mBitmapDisplayScaleRatio = mBitmapDisplayRect.width() / (float) mBitmap.getWidth();
        }
    }

    public void aimAtViewRect(int x, int y) {
        
        // should check if outof bitmap rect
        
        mAimPoint4ViewRect.set(x, y);
        mAimPoint4BitmapDisplayRect.set( x - mBitmapDisplayRect.left, y - mBitmapDisplayRect.top);
        
        updateCenter4ViewRect();
        mDrawable.setBounds(mCenter4ViewRect.x - mRadius, mCenter4ViewRect.y - mRadius, mCenter4ViewRect.x + mRadius, mCenter4ViewRect.y + mRadius);

        
        
        mShaderLocalMatrix.setTranslate(mRadius - mAimPoint4BitmapDisplayRect.x / mBitmapDisplayScaleRatio * mFactor, mRadius - mAimPoint4BitmapDisplayRect.y/ mBitmapDisplayScaleRatio * mFactor);
        mDrawable.getPaint().getShader().setLocalMatrix(mShaderLocalMatrix);
    }

    private void updateCenter4ViewRect() {
        int x, y, w, h;

        w = mMagnifierFrame.getWidth();
        h = mMagnifierFrame.getHeight();

        if (mAimPoint4ViewRect.x > w) {
            x = mAimPoint4ViewRect.x - w / 2;
        } else {
            x = mAimPoint4ViewRect.x + w / 2;
        }

        if (mAimPoint4ViewRect.y > h) {
            y = mAimPoint4ViewRect.y - h / 2;
        } else {
            y = h / 2;
        }

        mCenter4ViewRect.set(x, y);

    }

    public void draw(Canvas canvas) {
        if (!mVisible || mMagnifierFrame == null) {
            return;
        }

        mDrawable.draw(canvas);
        canvas.drawBitmap(mMagnifierFrame, mCenter4ViewRect.x - mMagnifierFrame.getWidth() / 2, mCenter4ViewRect.y
                - mMagnifierFrame.getHeight() / 2, null);
    }

    public void setVisible(boolean isVisible) {
        mVisible = isVisible;
    }

}
