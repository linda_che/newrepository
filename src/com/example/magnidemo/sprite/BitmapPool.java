package com.example.magnidemo.sprite;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapPool {

	Resources mResources;

	public BitmapPool(Resources res) {
		mResources = res;
	}

	public Bitmap getBitmapFor(String path) {
		return BitmapFactory.decodeFile(path);
	}

	public Bitmap getBitmapFor(int resId) {
		return BitmapFactory.decodeResource(mResources, resId);
	}
}
