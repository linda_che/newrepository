package com.example.magnidemo.sprite;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

/**
 * Represents a line
 * TODO There should be sprite hierarchy
 * @author Tian Yu, Cao Xiying
 * @version 0.3
 *
 */
public class Line {

	private final Path mPath = new Path();
	private Paint mPathPaint = new Paint();

	Dot[] mDots = new Dot[2];
	Stage mStage;

	private final int mStrokeAlpha = 128;

	public Line() {
		mPathPaint.setAntiAlias(true);
		mPathPaint.setDither(true);
		mPathPaint.setColor(Color.WHITE); // TODO this should be from the style
		mPathPaint.setAlpha(mStrokeAlpha);
		mPathPaint.setStyle(Paint.Style.STROKE);
		mPathPaint.setStrokeJoin(Paint.Join.ROUND);
		mPathPaint.setStrokeCap(Paint.Cap.ROUND);
	}

	public void setStage(Stage stg) {
		mStage = stg;
	}

	public void setDot1(Dot d) {
		mDots[1] = d;
	}

	public void setDot0(Dot d) {
		mDots[0] = d;
	}

	public void draw(Canvas canvas) {

		// draw line

		if (mDots[0] == null || mDots[1] == null) {
			return;
		}

		final int endRow = mDots[1].mCenter.x;
		final int startRow = mDots[0].mCenter.x;
		final int endColumn = mDots[1].mCenter.y;
		final int startColumn = mDots[0].mCenter.y;

		mPath.reset();
		mPath.moveTo(startRow, startColumn);
		mPath.lineTo(endRow, endColumn);
		canvas.drawPath(mPath, mPathPaint);

		// draw dots
		mDots[0].draw(canvas);
		mDots[1].draw(canvas);
	}

}
