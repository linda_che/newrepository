package com.example.magnidemo.sprite;

/**
 * TODO make a setting facility for measurement app
 * @author Tian Yu
 * @version 0.3
 *
 */
public class PseudoCommonSettings {
	// 280 ~ 240
	public static int MagnifierRadius = 120;
	public static int MagnifierFactor = 2;

	public PseudoCommonSettings() {
		// TODO Auto-generated constructor stub
	}

}
