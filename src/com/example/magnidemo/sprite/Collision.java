package com.example.magnidemo.sprite;

import com.example.magnidemo.engine.Core;

/**
 * Simple collision detection util. Maybe put this function in sprite later?
 * 
 * @author Tian Yu, Liu Yixi, Che Ronglu
 * @version 0.5
 * 
 */
public class Collision {

	Core mCore;

	public Collision(Core core) {
		mCore = core;
	}

	/**
	 * Get object hit by (x, y). Can be dot, line or tag.
	 * 
	 * @param x
	 *            x value from touch event
	 * @param y
	 *            y value from touch event
	 * @return a object who covers point(x, y). null if there is nothing at
	 *         point(x, y)
	 */
	public Object hit(int x, int y) {
		Object ret = null;

		// check dots
		for (Dot d : mCore.mDotStack) {
			if (isInCircle(d.mCenter.x, d.mCenter.y, x, y)) {
				return d;
			}
		}

		for (Line l : mCore.mLineSet) {
			if (isInCircle(x, y, l.mDots[0].mCenter.x, l.mDots[0].mCenter.y)) {
				return l.mDots[0];
			}
			if (isInCircle(x, y, l.mDots[1].mCenter.x, l.mDots[1].mCenter.y)) {
				return l.mDots[1];
			}
		}

		// check lines
		for (Line l : mCore.mLineSet) {
			if (isInSegment(l.mDots[0].mCenter.x, l.mDots[0].mCenter.y,
					l.mDots[1].mCenter.x, l.mDots[1].mCenter.y, x, y)) {
				return l;
			}
		}
		return ret;
	}

	/**
	 * The description: to judge whether the user touched the legal range of the
	 * circle
	 * 
	 * @param cx
	 *            :the center x
	 * @param cy
	 *            :the center y
	 * @param x
	 *            :the x of the user touched
	 * @param y
	 *            :the y of the user touched
	 * @return success return true else return false
	 */
	public boolean isInCircle(int cx, int cy, int x, int y) {
		float diffOfDot = (float) Math.sqrt(Math.pow((x - cx), 2)
				+ Math.pow((y - cy), 2));
		if (diffOfDot < 12.0f)
			return true;
		else
			return false;
	}

	/**
	 * The description: to judge whether the user touched the legal range of the
	 * segment
	 * 
	 * @param x0
	 *            :the start x of the line's first dot
	 * @param y0
	 *            :the start y of the line's first dot
	 * @param x1
	 *            :the stop x of the line's second dot
	 * @param y1
	 *            :the stop y of the line's second dot
	 * @param x
	 *            :the x of user touched
	 * @param y
	 *            :the y of user touched
	 * @if success return true else return false
	 */
	public boolean isInSegment(float x0, float y0, float x1, float y1, float x,
			float y) {
		float miny, maxy;
		float minx, maxx;
		if (y0 > y1) {
			miny = y1;
			maxy = y0;
			minx = x1;
			maxx = x0;
		} else {
			miny = y0;
			maxy = y1;
			minx = x0;
			maxx = x1;
		}
		float k = (miny - maxy) / (minx - maxx);
		float b = y1 - k * x1;
		// delta is the best error range for user
		float delta = 12;
		float tempx;
		if (minx > maxx) {
			tempx = minx;
			minx = maxx;
			maxx = tempx;
		}
		// Line is horizontal
		if (Math.abs(maxy - miny) <= 10) {
			if (x >= minx && x <= maxx
					&& (y >= miny - delta && y <= maxy + delta)) {
				return true;
			} else {
				return false;
			}
		}
		// Line isn't horizontal
		else if (y >= miny && y <= maxy) {
			// Line is vertical
			if ((Math.abs(x1 - x0) <= 10) && (x >= minx - delta)
					&& (x <= maxx + delta))
				return true;
			// Line is slash
			else {
				// put y into y = k*x + b, get xInLine
				float xInLine = (y - b) / k;
				if (Math.abs(x - xInLine) <= delta)
					return true;
				else
					return false;
			}
		}
		// none of the above
		else {
			return false;
		}
	}

}
