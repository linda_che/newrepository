// Generated from src/com/example/magnidemo/engine/DotsLines.g4 by ANTLR 4.2

package com.example.magnidemo.engine;


import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DotsLinesParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__4=1, T__3=2, T__2=3, T__1=4, T__0=5, DOT=6, LINE=7, INT=8, ADD=9, SUB=10, 
		WS=11;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "')'", "','", "'['", "'('", "DOT", "LINE", "INT", 
		"'+'", "'-'", "WS"
	};
	public static final int
		RULE_lines = 0, RULE_line = 1, RULE_insert_dot = 2, RULE_cancel_dot = 3, 
		RULE_dot = 4;
	public static final String[] ruleNames = {
		"lines", "line", "insert_dot", "cancel_dot", "dot"
	};

	@Override
	public String getGrammarFileName() { return "DotsLines.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		List mLineList = new ArrayList();
		List mActiveList = new ArrayList();                  

	public DotsLinesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LinesContext extends ParserRuleContext {
		public LinesContext lines() {
			return getRuleContext(LinesContext.class,0);
		}
		public LineContext line() {
			return getRuleContext(LineContext.class,0);
		}
		public TerminalNode SUB() { return getToken(DotsLinesParser.SUB, 0); }
		public TerminalNode ADD() { return getToken(DotsLinesParser.ADD, 0); }
		public LinesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lines; }
	}

	public final LinesContext lines() throws RecognitionException {
		return lines(0);
	}

	private LinesContext lines(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		LinesContext _localctx = new LinesContext(_ctx, _parentState);
		LinesContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_lines, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(20);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				System.out.println("empty set");
				}
				break;

			case 2:
				{
				setState(12); line();
				System.out.println("1st line to set");
				}
				break;

			case 3:
				{
				setState(15); match(4);
				setState(16); line();
				setState(17); match(1);
				System.out.println("1st line to set");
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(36);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(34);
					switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
					case 1:
						{
						_localctx = new LinesContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_lines);
						setState(22);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(24);
						switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
						case 1:
							{
							setState(23); match(ADD);
							}
							break;
						}
						setState(26); line();
						System.out.println("add a line to set");
						}
						break;

					case 2:
						{
						_localctx = new LinesContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_lines);
						setState(29);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(30); match(SUB);
						setState(31); line();
						/* don't make node */ System.out.println("remove a line from set");
						}
						break;
					}
					} 
				}
				setState(38);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class LineContext extends ParserRuleContext {
		public Cancel_dotContext cancel_dot() {
			return getRuleContext(Cancel_dotContext.class,0);
		}
		public List<TerminalNode> INT() { return getTokens(DotsLinesParser.INT); }
		public Insert_dotContext insert_dot(int i) {
			return getRuleContext(Insert_dotContext.class,i);
		}
		public TerminalNode INT(int i) {
			return getToken(DotsLinesParser.INT, i);
		}
		public List<Insert_dotContext> insert_dot() {
			return getRuleContexts(Insert_dotContext.class);
		}
		public TerminalNode LINE() { return getToken(DotsLinesParser.LINE, 0); }
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_line);
		try {
			setState(58);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(39); match(LINE);
				setState(40); match(5);
				setState(41); match(INT);
				setState(42); match(3);
				setState(43); match(INT);
				setState(44); match(3);
				setState(45); match(INT);
				setState(46); match(3);
				setState(47); match(INT);
				setState(48); match(2);
				System.out.println("recog line");
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(50); insert_dot();
				setState(51); insert_dot();
				System.out.println("line confirmed");
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(54); insert_dot();
				setState(55); cancel_dot();
				System.out.println("line cancelled");
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Insert_dotContext extends ParserRuleContext {
		public DotContext dot() {
			return getRuleContext(DotContext.class,0);
		}
		public TerminalNode ADD() { return getToken(DotsLinesParser.ADD, 0); }
		public Insert_dotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert_dot; }
	}

	public final Insert_dotContext insert_dot() throws RecognitionException {
		Insert_dotContext _localctx = new Insert_dotContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_insert_dot);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			_la = _input.LA(1);
			if (_la==ADD) {
				{
				setState(60); match(ADD);
				}
			}

			setState(63); dot();
			/* don't make node */ /*assert only the only 1 last unpaired dot can be cancelled*/ System.out.println("add dot");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cancel_dotContext extends ParserRuleContext {
		public DotContext dot() {
			return getRuleContext(DotContext.class,0);
		}
		public TerminalNode SUB() { return getToken(DotsLinesParser.SUB, 0); }
		public Cancel_dotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cancel_dot; }
	}

	public final Cancel_dotContext cancel_dot() throws RecognitionException {
		Cancel_dotContext _localctx = new Cancel_dotContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_cancel_dot);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66); match(SUB);
			setState(67); dot();
			/* don't make node */ /*assert only the only 1 last unpaired dot can be cancelled*/ System.out.println("cancel dot");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DotContext extends ParserRuleContext {
		public Token a;
		public Token b;
		public List<TerminalNode> INT() { return getTokens(DotsLinesParser.INT); }
		public TerminalNode DOT() { return getToken(DotsLinesParser.DOT, 0); }
		public TerminalNode INT(int i) {
			return getToken(DotsLinesParser.INT, i);
		}
		public DotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dot; }
	}

	public final DotContext dot() throws RecognitionException {
		DotContext _localctx = new DotContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_dot);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70); match(DOT);
			setState(71); match(5);
			setState(72); ((DotContext)_localctx).a = match(INT);
			setState(73); match(3);
			setState(74); ((DotContext)_localctx).b = match(INT);
			setState(75); match(2);
			int aValue = Integer.parseInt((((DotContext)_localctx).a!=null?((DotContext)_localctx).a.getText():null)); int bValue = Integer.parseInt((((DotContext)_localctx).b!=null?((DotContext)_localctx).b.getText():null)); System.out.println("recog dot(" + aValue + ", " + bValue + ")");
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0: return lines_sempred((LinesContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean lines_sempred(LinesContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 2);

		case 1: return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\rQ\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2"+
		"\27\n\2\3\2\3\2\5\2\33\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2%\n\2\f"+
		"\2\16\2(\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\5\3=\n\3\3\4\5\4@\n\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\2\3\2\7\2\4\6\b\n\2\2S\2\26\3\2\2"+
		"\2\4<\3\2\2\2\6?\3\2\2\2\bD\3\2\2\2\nH\3\2\2\2\f\r\b\2\1\2\r\27\b\2\1"+
		"\2\16\17\5\4\3\2\17\20\b\2\1\2\20\27\3\2\2\2\21\22\7\6\2\2\22\23\5\4\3"+
		"\2\23\24\7\3\2\2\24\25\b\2\1\2\25\27\3\2\2\2\26\f\3\2\2\2\26\16\3\2\2"+
		"\2\26\21\3\2\2\2\27&\3\2\2\2\30\32\f\4\2\2\31\33\7\13\2\2\32\31\3\2\2"+
		"\2\32\33\3\2\2\2\33\34\3\2\2\2\34\35\5\4\3\2\35\36\b\2\1\2\36%\3\2\2\2"+
		"\37 \f\3\2\2 !\7\f\2\2!\"\5\4\3\2\"#\b\2\1\2#%\3\2\2\2$\30\3\2\2\2$\37"+
		"\3\2\2\2%(\3\2\2\2&$\3\2\2\2&\'\3\2\2\2\'\3\3\2\2\2(&\3\2\2\2)*\7\t\2"+
		"\2*+\7\7\2\2+,\7\n\2\2,-\7\5\2\2-.\7\n\2\2./\7\5\2\2/\60\7\n\2\2\60\61"+
		"\7\5\2\2\61\62\7\n\2\2\62\63\7\4\2\2\63=\b\3\1\2\64\65\5\6\4\2\65\66\5"+
		"\6\4\2\66\67\b\3\1\2\67=\3\2\2\289\5\6\4\29:\5\b\5\2:;\b\3\1\2;=\3\2\2"+
		"\2<)\3\2\2\2<\64\3\2\2\2<8\3\2\2\2=\5\3\2\2\2>@\7\13\2\2?>\3\2\2\2?@\3"+
		"\2\2\2@A\3\2\2\2AB\5\n\6\2BC\b\4\1\2C\7\3\2\2\2DE\7\f\2\2EF\5\n\6\2FG"+
		"\b\5\1\2G\t\3\2\2\2HI\7\b\2\2IJ\7\7\2\2JK\7\n\2\2KL\7\5\2\2LM\7\n\2\2"+
		"MN\7\4\2\2NO\b\6\1\2O\13\3\2\2\2\b\26\32$&<?";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}