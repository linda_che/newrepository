package com.example.magnidemo.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import com.example.magnidemo.sprite.Dot;
import com.example.magnidemo.sprite.Line;
import com.example.magnidemo.sprite.Stage;

// TODO: antlr can not perform incremental parsing easly
// TODO: should change to javacc
public class Core {
	Stage mStage;
	public Stack<Dot> mDotStack = new Stack<Dot>();
	public Set<Line> mLineSet = new HashSet<Line>();

	public Core() {
		// TODO Auto-generated constructor stub
	}

	// public static void main(String[] args) {
	// try {
	// start();
	// } catch (Exception e) {
	// }
	// }
	public void setStage(Stage stg) {
		mStage = stg;
	}

	public static void start() throws IOException {
		InputStream is = System.in;
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String expr = br.readLine();
		// get first expression
		int line = 1;
		// track input expr line numbers

		DotsLinesParser parser = new DotsLinesParser(null); // share single
															// parser instance
		parser.setBuildParseTree(false);
		// don't need trees

		while (expr != null) {
			// while we have more expressions
			// create new lexer and token stream for each line (expression)
			ANTLRInputStream input = new ANTLRInputStream(expr + "\n");
			DotsLinesLexer lexer = new DotsLinesLexer(input);
			lexer.setLine(line);
			System.out.println("line set to " + line);
			// notify lexer of input position
			lexer.setCharPositionInLine(0);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			parser.setInputStream(tokens); // notify parser of new token stream
			parser.lines();
			// start the parser
			expr = br.readLine();
			// see if there's another line
			line++;
		}

	}

	public Object addDot(int x, int y) {

		Object ret = null;
		
		// TODO dot should be created in factory or stage object
		Dot d = new Dot();
		d.mCenter.set(x, y);
		d.setStage(mStage);

		if (mDotStack.isEmpty()) {
			mDotStack.push(d);
			ret = d;
		} else {
			Dot d0 = mDotStack.pop();
			Line l = pairDot(d0, d);
			ret = l;
		}
		
		mStage.invalidate();
		return ret;

	}

	private Line pairDot(Dot d0, Dot d1) {
		// TODO line should not be create here
		Line l = new Line();
		l.setStage(mStage);
		d0.setPaired(true);
		l.setDot0(d0);
		d1.setPaired(true);
		l.setDot1(d1);
		mLineSet.add(l);
		
		return l;
	}

	public void removeDot(Dot dot) {
		if (!dot.isPaired()) {
			if (!mDotStack.isEmpty() && mDotStack.peek() == dot) {
				mDotStack.pop();

				mStage.invalidate();
			}
		}
	}

	// TODO should don't be here?
	public void updateDot(Dot dot, int x, int y) {
		dot.mCenter.set(x, y);

		mStage.invalidate();

	}

	public void removeLine(Line line) {
		mLineSet.remove(line);

		mStage.invalidate();
	}

	public void removeAll(){
		
		mDotStack.clear();
		mLineSet.clear();
		mStage.invalidate();
		
	}
}
