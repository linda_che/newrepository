/*
 * only line and lines could make a node in AST
 */

grammar DotsLines;

@header {
package com.example.magnidemo.engine;

}

@parser::members {
	List mLineList = new ArrayList();
	List mActiveList = new ArrayList();                  
}

DOT : 'dot'
    | 'd'
    ;
LINE : 'line'
     | 'l'
     ;

INT : [1-9][0-9]* ;

ADD : '+' ;
SUB : '-' ;

WS : [ \r\t\n]+ -> skip ;

lines : {System.out.println("empty set");}
      | line {System.out.println("1st line to set");}
      | '[' line ']' {System.out.println("1st line to set");}
      | lines ADD? line {System.out.println("add a line to set");}
      | lines SUB line {/* don't make node */ System.out.println("remove a line from set");}
      ;


/* line can be sep into confirm line and cancel line */
line : LINE '(' INT ',' INT ',' INT ',' INT ')' {System.out.println("recog line");}
     | insert_dot insert_dot {System.out.println("line confirmed");}
     | insert_dot cancel_dot {System.out.println("line cancelled");}
     ;

insert_dot : ADD? dot {/* don't make node */ /*assert only the only 1 last unpaired dot can be cancelled*/ System.out.println("add dot");}
        ;

cancel_dot : SUB dot {/* don't make node */ /*assert only the only 1 last unpaired dot can be cancelled*/ System.out.println("cancel dot");}
           ;

dot : DOT '(' a = INT ',' b = INT ')' {int aValue = Integer.parseInt($a.text); int bValue = Integer.parseInt($b.text); System.out.println("recog dot(" + aValue + ", " + bValue + ")");}
    ;

