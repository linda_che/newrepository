package com.example.magnidemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.magnidemo.sprite.Stage;

/**
 * Main entry for measurement app
 * 
 * @author Tian Yu
 * @version 0.3
 * 
 */
public class MeasurementActivity extends Activity {

    Stage mStage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.measurement_activity_main);
        mStage = (Stage) findViewById(R.id.stage_view);
    }

    @Override
    public void onStart() {
        super.onStart();
        
        // get image url and init stage
        mStage.setDataSource(R.drawable.show);
        mStage.prepare();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_settings:
            break;
        case R.id.action_clear_all:
            mStage.clear();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

}
